import axios from 'axios';
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import envConfig from '../config/env.config';
import './main.css'
function Register() {
  const [userName , setUserName ] = useState("")
  const [email , setEmail ] = useState("")
  const [ password , setPassword ] = useState("")

  const onSubmit = e=>{
    e.preventDefault();


    if(userName==="" || email==="" || password===""){
      console.log("empty")
      return 
    }else{
      let obj = { username : userName ,
        email : email , 
        password : password}
    
      axios.post(`${envConfig.apiUrl}/auth/register`,obj)
      .then(res=>{
        console.log(res)
        window.open('/login', "_self")
      })
      .catch(err=>{
        console.log(err)
        // console.log(err.response.data.error)
        if(err.response && err.response.data.error){
          alert(err.response.data.error)
        }
      })
    }


  }
  return (
    <div>
      <h3 style={{textAlign:"center"}}>Register</h3>
       <form onSubmit={onSubmit} className="registerForm">
        <input type="text" placeholder="Name" onChange={e=>{setUserName(e.target.value)}}/>
        <input type="text" placeholder="password" onChange={e=>{setPassword(e.target.value)}}/>
        <input type="email" placeholder="email" onChange={e=>{setEmail(e.target.value)}}/>
        <button onClick={onSubmit}  >Submit</button>
      </form>
    </div>
  )
}

export default Register
