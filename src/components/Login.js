import axios from "axios";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import envConfig from "../config/env.config";
import './main.css'
function Login(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [cookies, setCookie, removeCookie, getCookie] = useCookies(["token"]);
  const [isLogged, setIsLogged] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .post(`${envConfig.apiUrl}/auth/login`, {
        username: userName,
        password: password,
      })
      .then((res) => {
        console.log(res);
        setCookie("token", res.data.access_token, { path: "/", maxAge: 120 });
        setIsLogged(true);
        props.checkLoggedIn(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (!props.isLogged) {
      removeCookie("token");
    }
  }, []);

  return (
    <div>
      <h3 style={{textAlign:"center"}}>Login</h3>
      <form onClick={onSubmit} className="loginForm">
        <input
          type="text"
          placeholder="Name"
          onChange={(e) => setUserName(e.target.value)}
        />
        <input
          type="text"
          placeholder="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <button onClick={onSubmit}>Submit</button>
      </form>
    </div>
  );
}

export default Login;
