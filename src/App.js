import "./App.css";
import Home from "./Home";


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Movie listing site</h2>
      </header>
      <Home></Home>
    </div>
  );
}

export default App;
