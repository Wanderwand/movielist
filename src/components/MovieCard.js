import React, { useEffect } from "react";
import { useState } from "react";
import "./movie.css";
function MovieCard(props) {
  const change = () => {
    if (props.changeFav) {
      props.changeFav(props.movie);
    } else {
      props.removeFormFav(props.i);
    }
  };

  return (
    <div className="movieCard">
      <div>
        <p>{props.movie.title}</p>
        <p>{props.movie.year}</p>
        <p>{props.movie.released}</p>
        <p>{props.movie.rating}</p>
      </div>
      <div className="star">

      {props.isFav ? (
        <button className="starButton" onClick={change}>&#9733;</button>
        ) : (
          <button className="starButton" onClick={change}>&#9734;</button>
          )}
          </div>
    </div>
  );
}

export default MovieCard;
