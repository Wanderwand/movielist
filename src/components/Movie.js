import axios from "axios";
import React, { useState } from "react";
import { useRef } from "react";
import envConfig from "../config/env.config";
import MovieCard from "./MovieCard";
import './movie.css'
function Movie() {
  const [movie, setMovie] = useState(null);
  const [favorites, setFavorites] = useState([]);
  const [isFav, setIsFav] = useState(false);
  let cancelToken;
  let timeout;

  const onChange = (e) => {
    let title = e.target.value;
    if (typeof cancelToken != typeof undefined) {
      cancelToken.cancel("Operation canceled due to new request.");
    }
    if (!!timeout) {
      clearTimeout(timeout);
    }
    cancelToken = axios.CancelToken.source();
    if (e.target.value === "") {
      setMovie(null);
      setIsFav(false)
      return;
    }else{
      setMovie(null);
      setIsFav(false)
    }
    timeout = setTimeout(() => {
      axios
        .get(`${envConfig.movieApiUrl}`, {
          params: {
            t: title,
            apikey: envConfig.movieApiKey,
          },
          cancelToken: cancelToken.token,
        })
        .then((res) => {
          console.log(res.data);
          if (res.data.Response === "True") {
            // console.log(movie);
            setMovie({
              title: res.data.Title,
              year: res.data.Year,
              released: res.data.Released,
              rating: res.data.imdbRating,
              // favorite: false,
            });
            console.log(res.data.Response);
          } else {
            setMovie(null);
            setIsFav(false)
          }
        })
        .catch((err) => {
          console.log(err);
          setMovie(null);
          setIsFav(false)
        });
    }, 2000);
  };

  const changeFav = (mov) => {
    let favs = favorites;
    if(favs.length>=1){
      if(favs[favs.length-1]===mov){
        favs.pop()
        setFavorites([...favs])
        setIsFav(false)
      }else{
        setFavorites([...favs ,mov])
        setIsFav(true)
      }
  }else{
    setFavorites([...favs ,mov])
    setIsFav(true)
  }
  };

  const removeFormFav = (i)=>{
    let favs = favorites;
    if(favs[i]===movie){
      changeFav(movie)
      return 
    }
    favs.splice(i,1)
    setFavorites([...favs])
  }
  return (
    <div className="container">
      <div>
        <h2>Search Movies below</h2>
      </div>
      <input type="text" onChange={onChange} />
      {/* {movie} */}
      {movie? (
        <div>

        <MovieCard movie={movie} changeFav={changeFav} isFav={isFav}></MovieCard>
        </div>
        
      ) : null }

        <hr/>

      {favorites.length>=1 ?  favorites.map((fav, i) => {
        return <MovieCard i={i} movie={fav} changeFav={null} removeFormFav={removeFormFav} isFav={true}></MovieCard>;
      }): null }
    </div>
  );
}

export default Movie;
