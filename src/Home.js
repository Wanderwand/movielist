import React, { useState } from "react";
import {
  Link,
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import Login from "./components/Login";
import Movie from "./components/Movie";
import Register from "./components/Register";
import "./home.css";

function Home() {
  const [isLogged, setIsLogged] = useState(false);

  const checkLoggedIn = (bool) => {
    if (bool) {
      setIsLogged(true);
    }
  };
  const logOut = () => {
    setIsLogged(false);
  };
  return (
    <div>
      <Router>
        <div className="navbar">
          <div>
            {!isLogged ? (
              <div>
                <span className="button">
                  <Link to="/login">Login</Link>
                </span>
                <span className="button">
                  <Link to="/register">Register</Link>
                </span>
              </div>
            ) : (
              <button onClick={logOut}>Log Out</button>
            )}
          </div>
        </div>
        <Switch>
            <Route path="/login">
            {!isLogged ? (
              <Login checkLoggedIn={checkLoggedIn} isLogged={isLogged} />
            ) : (
              <Redirect to="/" />
            )}
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          {/* <Route path="/">
              {!isLogged ? (
                <Redirect to="/register" />
              ) : (
                null
              )}
            </Route> */}
        </Switch>
      </Router>

      {isLogged ? (
        <div>
          <hr />
          <Movie></Movie>
        </div>
      ) : null}

      {/* <Movie></Movie> */}
    </div>
  );
}

export default Home;
